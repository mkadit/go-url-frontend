import {writable} from "svelte/store";

const MODE = process.env.NODE_ENV;
const IS_DEV = process.env.NODE_ENV === "development";

export const headers = new Headers({
    Accept: 'application/json',
    "Content-Type": "application/json",
});

export const BASE = IS_DEV
    ? "http://localhost:8081/api"
    : "https://backend.com/api";

export const API_URL = {
    GET_USER_URLS: BASE + "/user/urls",
    LOGIN: BASE + "/login",
    LOGOUT: BASE + "/logout",
    PUBLIC_URLS: BASE + "/urls",
    REGISTER: BASE + "/register",
    SHORT_URL: BASE + "/s",
    USER: BASE + "/user",
    USERS: BASE + "/users",
    HEADERS: headers,
    MODE: MODE

}


export const loginUser = writable([])
loginUser.set(0)
